import csv
from typing import List, Callable, Tuple, Dict, Set
from typing import NewType
from collections import defaultdict
from itertools import groupby
from functools import reduce
import operator


# 1. Identify the top 3 airlines which covers the maximum cities
# 2. Identify the top 3 airlines which have direct flight routes. (src to dst and dst to src should be counted as one)
# 3. List the top 10 cities in order of number of airlines servicing them

Route = NewType("Route", Tuple[str, str])


class AirlineRoute():
    airline_identifier: str
    route: Route
    codeshare: bool
    number_of_stops: int
    aircraft_types: List[str]

    def __init__(self, _airline_code, _airline_id, _source_airport,
                 _destination_airport, _codeshare, _number_of_stops,
                 _aircraft_types):
        self.airline_identifier = \
            "{}-{}".format(
                _airline_code, "0000" if _airline_id == "\\N" else _airline_id
            )
        # CODESMELL: This sorting should be done at runtime, but will affect performance
        self.route = Route(
            tuple(sorted((_source_airport, _destination_airport)))
        )
        self.codeshare = True if _codeshare.lower() == "y" else False
        self.number_of_stops = int(_number_of_stops)
        self.aircraft_types = _aircraft_types.split(" ")

    def __repr__(self):
        return """
AirlineRoute(airline_identifier={}, route={}, codeshare={},number_of_stops={}, aircraft_types={})
""".strip().format(self.airline_identifier, self.route, self.codeshare, self.number_of_stops, self.aircraft_types)


AirlineRoutes = NewType("AirlineRoutes", List[AirlineRoute])


def load_data(data_file: str) -> AirlineRoutes:
    routes_data = list()
    with open(data_file) as df:
        df_reader = csv.reader(df)
        routes_data.extend(
            [AirlineRoute(r[0], r[1], r[2], r[4], r[6], r[7], r[8])
             for r in df_reader]
        )
    return AirlineRoutes(routes_data)


def _group_by_airline(data: AirlineRoutes):

    def airline_identifier_key_fn(ar: AirlineRoute) -> str:
        return ar.airline_identifier

    sorted_routes = sorted(data, key=airline_identifier_key_fn)
    return groupby(sorted_routes, key=airline_identifier_key_fn)


def city_coverage_summary(data: AirlineRoutes) -> List[Tuple[str, int]]:
    grouped_routes: Dict[str, AirlineRoutes]
    grouped_routes = _group_by_airline(data)

    airline_and_routes: List[Tuple[str, List[Route]]]
    airline_and_routes = [(airline_identifier,
                           list(set([ar.route for ar in list(airline_routes)])))
                          for airline_identifier, airline_routes in grouped_routes]

    airline_and_cities: List[Tuple[str, List[str]]]
    airline_and_cities = [(airline_identifier,
                           list(set(reduce(operator.concat, routes))))
                          for airline_identifier, routes in airline_and_routes]

    airline_and_city_counts: List[Tuple[str, int]]
    airline_and_city_counts = [(airline_identifier, len(cities))
                               for airline_identifier, cities in airline_and_cities]
    return sorted(airline_and_city_counts, key=lambda x: x[1], reverse=True)


def direct_flights_summary(data: AirlineRoutes) -> List[Tuple[str, List[Route], int]]:
    direct_flights = list(filter(lambda x: x.number_of_stops == 0, data))

    grouped_routes: Dict[str, AirlineRoutes]
    grouped_routes = _group_by_airline(direct_flights)

    airline_and_routes: List[Tuple[str, List[Route]]]
    airline_and_routes = [(airline_identifier,
                           list(set([ar.route for ar in airline_routes])))
                          for airline_identifier, airline_routes in grouped_routes]

    airline_routes_and_route_counts: List[Tuple[str, List[Route], int]]
    airline_routes_and_route_counts = [(airline_identifier, routes, len(routes))
                                       for airline_identifier, routes in airline_and_routes]

    return sorted(airline_routes_and_route_counts, key=lambda x: x[2], reverse=True)


def _group_by_route(data: AirlineRoutes):

    def route_key_fn(ar: AirlineRoute) -> Route:
        return ar.route

    sorted_data = sorted(data, key=route_key_fn)
    return groupby(sorted_data, key=route_key_fn)


def cities_with_max_airlines_summary(data: AirlineRoutes) -> List[Tuple[str, Set[str], int]]:

    def _group_cities_and_airlines(data: List[Tuple[str, str, Set[str]]],
                                   groupbyFn: Callable[[Tuple[str, str, Set[str]]], str]):
        sorted_data = sorted(data, key=groupbyFn)
        return groupby(sorted_data, key=groupbyFn)

    grouped_routes: Dict[Route, AirlineRoutes]
    grouped_routes = _group_by_route(data)

    route_cities_and_airlines = [(route[0], route[1],
                                  set([ar.airline_identifier for ar in list(airline_routes)]))
                                 for route, airline_routes in grouped_routes]

    list1 = _group_cities_and_airlines(route_cities_and_airlines,
                                       lambda x: x[0])
    list2 = _group_cities_and_airlines(route_cities_and_airlines,
                                       lambda x: x[1])

    list1_cities_and_airlines = [(city,
                                  set(reduce(operator.concat, [list(x[2]) for x in list(d)])))
                                 for city, d in list1]
    list2_cities_and_airlines = [(city,
                                  set(reduce(operator.concat, [list(x[2]) for x in list(d)])))
                                 for city, d in list2]

    cities_and_airlines: Dict[str, Set[str]]
    cities_and_airlines = defaultdict(set)
    for c, a in list1_cities_and_airlines:
        cities_and_airlines[c].update(a)
    for c, a in list2_cities_and_airlines:
        cities_and_airlines[c].update(a)

    return sorted([(city, airlines, len(airlines))
                   for city, airlines in cities_and_airlines.items()],
                  key=lambda x: x[2], reverse=True)


if __name__ == "__main__":
    log_separator_line = "--**--" * 15
    data = load_data("0012-airline-routes.dat")

    for d in city_coverage_summary(data)[:5]:
        print(d)
    print(log_separator_line)
    for d0, _, d2 in direct_flights_summary(data)[:5]:
        print("{} -> {}".format(d0, d2))
    print(log_separator_line)
    for d0, d1, d2 in cities_with_max_airlines_summary(data)[:10]:
        print("{} -> {}".format(d0, d2))
    print(log_separator_line)
